# thumbi

A soothing video player for those who wants to engage with moving images.

Find it in use at [thumbi.netlify.app](https://thumbi.netlify.app) and [sofiamerelli.com](https://sofiamerelli.com).

## About

The effect is a feedback loop that samples a video frame and draw it back in the canvas. Some parameters can be manipulated: the `scale` of the thumbnail, the `zoom` intensity, and the `source` video file. 

It's made with the [canvas API](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API), [Vue](https://vuejs.org/) and [Vite](https://vitejs.dev/).

## Video Thumbnail

For best results use small resolution video files, such as `320p`, `480p` and so on. The lower the better. Tested a bit with Chromium and it runs smoothly `720p` files, even `1280p` ones. Firefox however it's a disaster with resolutions greater than `480p`.

Find here a ffmpeg snippet to convert - resize - trim - mute video files:

```
ffmpeg -i input.mp4 -c:v libx264 -pix_fmt yuv420p -crf 28 -preset fast -tune zerolatency -an -vf scale=-2:360 output.mp4
```

At the moment, the app takes only online video file url, but will make a upload button as well soon.

## License

km0 - thumbi - 2024 
Copyleft with a difference: This is a collective work, you are invited to copy, distribute, and modify it under the terms of the [CC4r](https://constantvzw.org/wefts/cc4r.en.html).
